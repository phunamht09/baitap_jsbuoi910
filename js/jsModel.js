// /**
// Mô tả: Xây dựng mảng nhân viên và các phương thức
// Người tạo: Phu Nam
// Ngay tao: 29-12-2022
// Version: 1.0
//  */

//tao lop doi tuong
function NhanVien(
    _tkNV,
    _tenNV,
    _emailNV,
    _passwordNV,
    _dateLV,
    _luongCB,
    _chucVu,
    _gioLam,
) {
    this.tkNV = _tkNV;
    this.tenNV = _tenNV;
    this.emailNV = _emailNV;
    this.passwordNV = _passwordNV;
    this.dateLV = _dateLV;
    this.luongCB = _luongCB;
    this.chucVu = _chucVu;
    this.gioLam = _gioLam;

    //5. Xây dựng phương thức tính tổng lương theo chức vụ
    this.tongLuong = function() {
        if (this.chucVu == "Sếp") {
            return (this.luongCB * 3).toLocaleString();
        } else if (this.chucVu == "Trưởng phòng") {
            return(this.luongCB * 2).toLocaleString();
        } else if (this.chucVu == "Nhân viên") {
            return(this.luongCB * 1).toLocaleString();
        }
    };

    //6. Xây dựng phương thức xếp loại cho nhân viên theo số giờ làm
    this.xepLoai = function() {
        var xepLoai = "";
        if (this.chucVu == "Nhân viên" && this.gioLam >= 192) {
            return xepLoai = "Nhân viên xuất sắc";
        } else if (this.chucVu == "Nhân viên" && this.gioLam >= 176) {
            return xepLoai = "Nhân viên giỏi";
        } else if (this.chucVu == "Nhân viên" && this.gioLam >= 160) {
            return xepLoai = "Nhân viên khá";
        } else if (this.chucVu == "Nhân viên" && this.gioLam < 160) {
            return xepLoai = "Nhân viên trung bình";
        } else if (this.chucVu == "Sếp" || this.chucVu == "Trưởng phòng")
            return xepLoai = "";
    };
};

