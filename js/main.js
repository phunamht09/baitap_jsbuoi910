// /**
//  * Mô tả: Dự án Quản lý sinh viên
//  * Nguoi tao: Nguyen Phu Nam
//  * Ngay tao: 29-12-2022
//  * Version: 1.0
//  */

var data = [];


// 2. Thêm nhân viên mới
function btnThemNguoiDung() {
    var nv = layThongTinForm();

    //validate hop le
    var isValid = true;
    isValid = kiemTraHopLe();
    if (isValid) {
        data.push(nv);
        renderDSNV(data);
    }
}

//Xóa nhân viên
function xoaNV(idNV) {
    for (var i = 0; i < data.length; i++) {
        if (data[i].tkNV == idNV) {
            data.splice(i,1)

            renderDSNV(data);
        }
    }
}

// //Sửa nhân viên
function suaNV(idNV) {
    for (var i = 0; i < data.length; i++) {
        if (data[i].tkNV == idNV) {
            document.getElementById("tknv").disabled = true;
        }
    }
}