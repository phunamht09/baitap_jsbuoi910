/**
 * Mô tả: Tao đối tượng nhân viên và in ra bảng dữ liệu
 * Nguoi tao: Nguyen Phu Nam
 * Ngay tao: 29-12-2022
 * Version: 1.0
 */

// 3. Tạo đối tượng nhân viên từ thông tin nhập vào từ form
function layThongTinForm() {
    var _tkNV = document.getElementById("tknv").value;
    var _tenNV = document.getElementById("ten").value;
    var _emailNV = document.getElementById("email").value;
    var _passwordNV = document.getElementById("password").value;
    var _dateLV = document.getElementById("datepicker").value;
    var _luongCB = document.getElementById("luongCB").value * 1;
    var _chucVu = document.getElementById("chucvu").value;
    var _gioLam = document.getElementById("gioLam").value * 1;

    return new NhanVien(
        _tkNV,
        _tenNV,
        _emailNV,
        _passwordNV,
        _dateLV,
        _luongCB,
        _chucVu,
        _gioLam,
    );
}


// 1. In ra table danh sách nhân viên
function renderDSNV(nvArr) {
var contentHTML = "";
for (var index = 0; index < nvArr.length; index++) {
    var nv = nvArr[index];

    var contentTr = `<tr>
    <td>${nv.tkNV}</td>
    <td>${nv.tenNV}</td>
    <td>${nv.emailNV}</td>
    <td>${nv.dateLV}</td>
    <td>${nv.chucVu}</td>
    <td>${nv.tongLuong()}</td>
    <td>${nv.xepLoai()}</td>
    <td>
    <button class="btn btn-danger" onclick ="xoaNV('${nv.tkNV}')">Xóa</button>
    <button class="btn btn-warning" onclick ="suaNV('${nv.tkNV}')">Sửa</button>
    </td>
    </tr>`;
    contentHTML = contentHTML + contentTr;
}
//show ra bang hien thi
document.getElementById("tableDanhSach").innerHTML = contentHTML;
}

// Tìm kiếm vị trí nhân viên
function timKiemViTri(id, data) {
var viTri = -1;
for (var index = 0; index < data.length; index++) {
    if (data[index].tkNV == id) {
        viTri = index;
    }
}
return viTri;
}
